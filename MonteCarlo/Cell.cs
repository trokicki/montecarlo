﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarlo {
	class Cell {
		public int [] Location { get; set; }
		public int GermID { get; set; }
		public bool OrientationChangeProposed { get; private set; }

		public Cell(int x, int y) {
			Location = new int[] { x, y };
			OrientationChangeProposed = false;
			GermID = 0;
		}

		public void ProposeOrientationChange() {
			OrientationChangeProposed = true;
		}
	}
}
