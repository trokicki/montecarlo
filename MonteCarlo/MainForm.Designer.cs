﻿namespace MonteCarlo {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.DrawingPanel = new System.Windows.Forms.Panel();
			this.GermsListView = new System.Windows.Forms.ListView();
			this.GermsAmountTextBox = new System.Windows.Forms.TextBox();
			this.GenerateGermsButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.StartSimulationButton = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.DrawStepNUD = new System.Windows.Forms.NumericUpDown();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.NoneEndingConditionRadio = new System.Windows.Forms.RadioButton();
			this.EachCellAskedEndingConditionRadio = new System.Windows.Forms.RadioButton();
			((System.ComponentModel.ISupportInitialize)(this.DrawStepNUD)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// DrawingPanel
			// 
			this.DrawingPanel.BackColor = System.Drawing.Color.Transparent;
			this.DrawingPanel.Location = new System.Drawing.Point(226, 5);
			this.DrawingPanel.Name = "DrawingPanel";
			this.DrawingPanel.Size = new System.Drawing.Size(570, 570);
			this.DrawingPanel.TabIndex = 0;
			// 
			// GermsListView
			// 
			this.GermsListView.Location = new System.Drawing.Point(15, 39);
			this.GermsListView.Name = "GermsListView";
			this.GermsListView.Size = new System.Drawing.Size(200, 382);
			this.GermsListView.TabIndex = 1;
			this.GermsListView.UseCompatibleStateImageBehavior = false;
			this.GermsListView.View = System.Windows.Forms.View.SmallIcon;
			// 
			// GermsAmountTextBox
			// 
			this.GermsAmountTextBox.Location = new System.Drawing.Point(96, 12);
			this.GermsAmountTextBox.Name = "GermsAmountTextBox";
			this.GermsAmountTextBox.Size = new System.Drawing.Size(38, 20);
			this.GermsAmountTextBox.TabIndex = 2;
			this.GermsAmountTextBox.Text = "10";
			// 
			// GenerateGermsButton
			// 
			this.GenerateGermsButton.Location = new System.Drawing.Point(140, 10);
			this.GenerateGermsButton.Name = "GenerateGermsButton";
			this.GenerateGermsButton.Size = new System.Drawing.Size(75, 23);
			this.GenerateGermsButton.TabIndex = 3;
			this.GenerateGermsButton.Text = "Generate";
			this.GenerateGermsButton.UseVisualStyleBackColor = true;
			this.GenerateGermsButton.Click += new System.EventHandler(this.GenerateGermsButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(78, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Germs amount:";
			// 
			// StartSimulationButton
			// 
			this.StartSimulationButton.Location = new System.Drawing.Point(127, 540);
			this.StartSimulationButton.Name = "StartSimulationButton";
			this.StartSimulationButton.Size = new System.Drawing.Size(88, 35);
			this.StartSimulationButton.TabIndex = 5;
			this.StartSimulationButton.Text = "Start / pause simulation";
			this.StartSimulationButton.UseVisualStyleBackColor = true;
			this.StartSimulationButton.Click += new System.EventHandler(this.StartSimulationButton_Click);
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Transparent;
			this.label2.Location = new System.Drawing.Point(12, 427);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(159, 34);
			this.label2.TabIndex = 6;
			this.label2.Text = "Steps drawing gap [increase to increase performance]";
			// 
			// DrawStepNUD
			// 
			this.DrawStepNUD.Location = new System.Drawing.Point(177, 427);
			this.DrawStepNUD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.DrawStepNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.DrawStepNUD.Name = "DrawStepNUD";
			this.DrawStepNUD.Size = new System.Drawing.Size(38, 20);
			this.DrawStepNUD.TabIndex = 7;
			this.DrawStepNUD.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
			this.DrawStepNUD.ValueChanged += new System.EventHandler(this.DrawStepNUD_ValueChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.EachCellAskedEndingConditionRadio);
			this.groupBox1.Controls.Add(this.NoneEndingConditionRadio);
			this.groupBox1.Location = new System.Drawing.Point(15, 464);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(200, 69);
			this.groupBox1.TabIndex = 9;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Ending condition";
			// 
			// NoneEndingConditionRadio
			// 
			this.NoneEndingConditionRadio.AutoSize = true;
			this.NoneEndingConditionRadio.Checked = true;
			this.NoneEndingConditionRadio.Location = new System.Drawing.Point(6, 19);
			this.NoneEndingConditionRadio.Name = "NoneEndingConditionRadio";
			this.NoneEndingConditionRadio.Size = new System.Drawing.Size(51, 17);
			this.NoneEndingConditionRadio.TabIndex = 0;
			this.NoneEndingConditionRadio.TabStop = true;
			this.NoneEndingConditionRadio.Text = "None";
			this.NoneEndingConditionRadio.UseVisualStyleBackColor = true;
			this.NoneEndingConditionRadio.CheckedChanged += new System.EventHandler(this.NoneEndingConditionRadio_CheckedChanged);
			// 
			// EachCellAskedEndingConditionRadio
			// 
			this.EachCellAskedEndingConditionRadio.AutoSize = true;
			this.EachCellAskedEndingConditionRadio.Location = new System.Drawing.Point(6, 42);
			this.EachCellAskedEndingConditionRadio.Name = "EachCellAskedEndingConditionRadio";
			this.EachCellAskedEndingConditionRadio.Size = new System.Drawing.Size(178, 17);
			this.EachCellAskedEndingConditionRadio.TabIndex = 2;
			this.EachCellAskedEndingConditionRadio.Text = "Each cell asked to change state";
			this.EachCellAskedEndingConditionRadio.UseVisualStyleBackColor = true;
			this.EachCellAskedEndingConditionRadio.CheckedChanged += new System.EventHandler(this.EachCellAskedEndingConditionRadio_CheckedChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(803, 583);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.DrawStepNUD);
			this.Controls.Add(this.StartSimulationButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.GenerateGermsButton);
			this.Controls.Add(this.GermsAmountTextBox);
			this.Controls.Add(this.GermsListView);
			this.Controls.Add(this.DrawingPanel);
			this.Controls.Add(this.label2);
			this.Name = "MainForm";
			this.Text = "MonteCarlo";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.DrawStepNUD)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel DrawingPanel;
		private System.Windows.Forms.ListView GermsListView;
		private System.Windows.Forms.TextBox GermsAmountTextBox;
		private System.Windows.Forms.Button GenerateGermsButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button StartSimulationButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown DrawStepNUD;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton EachCellAskedEndingConditionRadio;
		private System.Windows.Forms.RadioButton NoneEndingConditionRadio;
	}
}

