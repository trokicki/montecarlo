﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarlo {
	public partial class MainForm : Form {
		private const int rows = 56;
		private const int columns = rows;
		private List<Germ> germs = new List<Germ>();
		private Cell[,] cells = new Cell[rows, columns];
		private Size cellSize = new Size(10, 10);
		private int[] cellPaddings = new int[] { 0, 0 };
		private Random random = new Random();
		bool working = false;
		int drawCount = 0;
		int drawEachNSteps = 10;

		private enum EndingCondition { NONE, EACH_CELL_ASKED };

		private EndingCondition endingCondition = EndingCondition.NONE;

		public MainForm() {
			InitializeComponent();
			initializeCellsArray();
		}

		private void GenerateGermsButton_Click(object sender, EventArgs e) {
			int germsAmount = 0;
			if (Int32.TryParse(GermsAmountTextBox.Text, out germsAmount)) {
				Random random = new Random();
				germs.Clear();
				for (int i = 0; i < germsAmount; i++) {
					germs.Add(new Germ() {
						Color = Color.FromArgb(random.Next(220), random.Next(220), random.Next(220))
					});
				}
				populateGermsToListView();
				fillAreaRandomly();
				drawCells();
			}
		}

		private void populateGermsToListView() {
			GermsListView.Clear();
			GermsListView.Items.AddRange(germs.Select(g => new ListViewItem() { Text = g.ToString() }).ToArray());
		}

		private void fillAreaRandomly() {
			foreach (Cell cell in cells) {
				cell.GermID = getRandomGerm().ID;
			}
		}

		private void initializeCellsArray() {
			for (int row = 0; row < cells.GetLength(0); row++) {
				for (int column = 0; column < cells.GetLength(1); column++) {
					cells[row, column] =  new Cell(row * cellSize.Width, column * cellSize.Height);
				}
			}
		}

		private void TEMP_CELLS_MOCK() {
			cells[3, 3].GermID = 1;

			cells[2, 2].GermID = 1;
			cells[2, 3].GermID = 5;
			cells[2, 4].GermID = 1;
			cells[3, 2].GermID = 3;
			cells[3, 4].GermID = 1;
			cells[4, 2].GermID = 4;
			cells[4, 3].GermID = 1;
			cells[4, 4].GermID = 2;
		}

		private void drawCells() {
			foreach (var cell in cells) {
				drawCell(cell);
			}
		}

		private void drawCell(Cell cell) {
			Color germColor = Color.LightGray;
			if (cell.GermID > 0 && germs.Any()) {
				germColor = germs.Where(z => z.ID == cell.GermID).Select(z => z.Color).First();
			}
			using (SolidBrush grayBrush = new SolidBrush(Color.Gray))
			using (SolidBrush cellBrush = new SolidBrush(germColor))
			using (Graphics graphics = DrawingPanel.CreateGraphics()) {
				Rectangle cellRectangle = new Rectangle(
					cell.Location[0] + cellPaddings[0],
					cell.Location[1] + cellPaddings[1],
					cellSize.Width - cellPaddings[0],
					cellSize.Height - cellPaddings[1]);
				if (cellBrush != null)
					graphics.FillRectangle(cellBrush, cellRectangle);
			}
		}

		Stopwatch stopwatch = new Stopwatch();
		private async void StartSimulationButton_Click(object sender, EventArgs e) {
			//TEMP_mock_full_cells_array();
			if (working) {
				stopwatch.Stop();
				working = false;
			} else {
				stopwatch.Start();
				drawEachNSteps = (int)DrawStepNUD.Value;
				drawCells();
				if (germs.Count == 0) {
					MessageBox.Show("No germs defined.");
					return;
				}
				working = true;
				await simulation();
			}
		}

		private async Task simulation() {
			while (working) {
				await Task.Run(() => simulationStep());
			}
		}

		private void simulationStep() {
			int row = random.Next(rows);
			int column = random.Next(columns);
			int randomGermId = getRandomGerm().ID;
			int currentCellEnergy = HamiltonParam(row, column);
			int newCellEnergy = HamiltonParam(row, column, randomGermId);
			cells[row, column].ProposeOrientationChange();
			if (cellShouldChange(newCellEnergy - currentCellEnergy)) {
				cells[row, column].GermID = randomGermId;
			}
			redrawCellsIfNecessary();

			if (checkIfSimulationComplete()) {
				working = false;
				MessageBox.Show(String.Format("Complete. Total time elapsed: {0}", stopwatch.Elapsed));
			}
		}

		private void redrawCellsIfNecessary() {
			if (++drawCount >= drawEachNSteps) {
				drawCells();
				drawCount = 0;
			}
		}

		private const double hardCodedKT = 0.6;
		private bool cellShouldChange(int cellEnergy) {
			if (cellEnergy <= 0)
				return true;
			double probability = Math.Exp(-(double)cellEnergy / hardCodedKT);
			double rand = random.NextDouble();
			if (rand <= probability) {
				return true;
			}
			return false;
		}
			
		private bool checkIfSimulationComplete() {
			switch (endingCondition) {
				case EndingCondition.EACH_CELL_ASKED:
					foreach (Cell cell in cells) {
						if (!cell.OrientationChangeProposed)
							return false;
					}
					return true;
				case EndingCondition.NONE:
					return false;
			}
			return false;
		}

		private Germ getRandomGerm() {
			return germs[random.Next(germs.Count)];
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
			working = false;
		}

		private List<Cell> cellsNeighbours(int row, int column) {
			List<Cell> output = new List<Cell>();
			if (row > 0) {
				if(column > 0)
					output.Add(cells[row - 1, column - 1]);
				output.Add(cells[row - 1, column]);
				if (column < cells.GetUpperBound(1))
					output.Add(cells[row - 1, column + 1]);
			}
			if(column > 0)
				output.Add(cells[row, column - 1]);
			if(column < cells.GetUpperBound(1))
				output.Add(cells[row, column + 1]);
			if (row < cells.GetUpperBound(0)) {
				if (column > 0)
					output.Add(cells[row + 1, column - 1]);
				output.Add(cells[row + 1, column]);
				if (column < cells.GetUpperBound(1))
					output.Add(cells[row + 1, column + 1]);
			}

			return output;
		}

		private int HamiltonParam(int row, int column) {
			Cell cell = cells[row, column];
			List<Cell> neighbours = cellsNeighbours(row, column);
			int output = 
			neighbours.Count(n => n.GermID != cell.GermID);
			return output;
		}

		private int HamiltonParam(int row, int column, int newGermId) {
			List<Cell> neighbours = cellsNeighbours(row, column);
			return neighbours.Count(n => n.GermID != newGermId);
		}

		private void DrawStepNUD_ValueChanged(object sender, EventArgs e) {
			drawEachNSteps = (int)DrawStepNUD.Value;
		}

		private void NoneEndingConditionRadio_CheckedChanged(object sender, EventArgs e) {
			setEndingCondition();
		}

		private void EachCellAskedEndingConditionRadio_CheckedChanged(object sender, EventArgs e) {
			setEndingCondition();
		}

		private void setEndingCondition() {
			if (NoneEndingConditionRadio.Checked) {
				endingCondition = EndingCondition.NONE;
			} else {
				endingCondition = EndingCondition.EACH_CELL_ASKED;
			}
		}
	}
}
