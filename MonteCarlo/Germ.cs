﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarlo {
	class Germ {
		private static int germsCount = 0;
		public int ID { get; private set; }
		public Color Color { get; set; }

		public Germ() {
			ID = ++germsCount;
		}

		public override string ToString() {
			return String.Format("Germ #{0} [{1}, {2}, {3}]", ID, Color.R, Color.G, Color.B);
		}
	}
}
